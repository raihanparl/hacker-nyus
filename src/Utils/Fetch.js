const DEBUG = true;
const res_url ="https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2Fwwwid";
export function HTTPRequest(method, body, header, successHandler, errorHandler) {
  if (DEBUG) {
    console.log('BODY:', body);
  }

  fetch(res_url, {
    method: method,
    // credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    body,
  })
    .then(response => {
      response.json().then(json => {
        if (DEBUG) {
          console.log(json);
        }
        successHandler(json);
      }).catch(err => {
        if (DEBUG) {
          console.log(err);
        }
        successHandler(err);
      });
    })
    .catch(err => {
      if (errorHandler) {
        if (DEBUG) {
          console.log('error', err);
        }
        errorHandler(err);
      }
    });
}

export default class Fetch {
  static GET(header, successHandler, errorHandler) {
    HTTPRequest('GET', null, header, successHandler, errorHandler);
  }

  static POST(body, header, successHandler, errorHandler) {
    HTTPRequest('POST', body, header, successHandler, errorHandler);
  }

  static PUT(body, header, successHandler, errorHandler) {
    HTTPRequest('PUT', body, header, successHandler, errorHandler);
  }

  static DELETE(successHandler, errorHandler) {
    HTTPRequest('DELETE', null, false, null, successHandler, errorHandler);
  }
}