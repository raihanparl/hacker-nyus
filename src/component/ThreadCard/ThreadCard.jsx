import React, {Component} from 'react';
import './thread-card.css';
const regex = /(<([^>]+)>)/ig;


class ThreadCard extends Component {
  renderDescription(description) {
    if (description.length <= 250) {
      return `${description}...`;
    } else {
      const shortDescription = description.substring(0, 250);
      const lastSpace = shortDescription.lastIndexOf(' ');
      return `${shortDescription.substring(0, lastSpace)}...`;
    }
  }

  render() {
    const {thread} = this.props;
    return (
      <div className="thread-card">
        <div className="card-image">
            <img
              className="thread-image"
              src={thread.thumbnail ? thread.thumbnail : 'https://bulma.io/images/placeholders/640x480.png'}
              alt="Placeholder for thread card"/>
        </div>
        <div className="card-content">

          <div className="header">
            <h3>{thread.title}</h3>
          </div>
          {/*dangerouslySetInnerHTML={{ __html:}}*/}
          <div className="content" >
            {this.renderDescription(this.props.thread.description.replace(regex, ''))}}
          </div>
        </div>
      </div>
    );
  }
}

export default ThreadCard;